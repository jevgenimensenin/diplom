package ttu.jevgeni.organizer.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.StudentSubject;

@Repository
@Transactional
public class StudentSubjectDaoImpl implements StudentSubjectDao{
	
	@PersistenceContext
	private EntityManager em;

	public StudentSubject getStudentSubjectById(Long studentSubjectId) {
		return em.createQuery("from StudentSubject where studentSubjectId = :studentSubjectId", StudentSubject.class)
				.setParameter("studentSubjectId", studentSubjectId).getSingleResult();
	}	

}
