package ttu.jevgeni.organizer.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Task;

@Repository
@Transactional
public class AttachedSubjectDaoImpl implements AttachedSubjectDao{
	
	@PersistenceContext
	private EntityManager em;

	public void saveTasksToSubject(Task task){
		em.persist(task);
	}

	public AttachedSubject getAttachedSubjectById(Long attachedSubjectId) {
		return em.createQuery("from AttachedSubject where attachedSubjectId = :attachedSubjectId", AttachedSubject.class)
				.setParameter("attachedSubjectId", attachedSubjectId).getSingleResult();
	}

	public AttachedSubject setCommentToSubject(AttachedSubject attachedSubject) {
		return em.merge(attachedSubject);
	}

	public List<AttachedSubject> getAllAttachedSubjects() {
		return em.createQuery("from AttachedSubject", AttachedSubject.class).getResultList();
	}
	

}
