package ttu.jevgeni.organizer.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.Account;

@Repository
@Transactional
public class AccountDaoImpl implements AccountDao{
	
	@PersistenceContext
	private EntityManager em;


	public Account getAccountById(long accountId) {
		return em.createQuery("from Account where accountId = :accountId", Account.class)
				.setParameter("accountId", accountId).getSingleResult();
	}


	public Account getAccountByEmail(String email) {
		return em.createQuery("from Account where email = :email", Account.class)
				.setParameter("email", email).getSingleResult();
	}
	
	
	
}
