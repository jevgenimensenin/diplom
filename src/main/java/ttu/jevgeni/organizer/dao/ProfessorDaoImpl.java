package ttu.jevgeni.organizer.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Subject;

@Repository
@Transactional
public class ProfessorDaoImpl implements ProfessorDao{
	
	@PersistenceContext
	private EntityManager em;

	public List<AttachedSubject> getAllProfessorSubjects(long professorId) {
		Query query = em.createQuery("from AttachedSubject where professorId=:professorId").setParameter("professorId", professorId);
		return query.getResultList();
	}

	public void saveSubjectWithThisProfessor(Account professor, Subject subject) throws Exception{
		AttachedSubject attachedSubject = new AttachedSubject();
		attachedSubject.setProfessor(professor);
		attachedSubject.setSubject(subject);
		em.persist(attachedSubject);
	}

	public AttachedSubject getAttachedSubjectById(long attachedSubjectId) {
		return em.createQuery("from AttachedSubject where attachedsubjectid=:attachedSubjectId", AttachedSubject.class).
		setParameter("attachedSubjectId", attachedSubjectId).getSingleResult();
	}
	
	

}
