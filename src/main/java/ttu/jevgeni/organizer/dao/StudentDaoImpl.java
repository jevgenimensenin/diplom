package ttu.jevgeni.organizer.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.StudentSubject;
import ttu.jevgeni.organizer.model.Subject;

@Repository
@Transactional
public class StudentDaoImpl implements StudentDao{
	
	@PersistenceContext
	private EntityManager em;

	public void saveSubjectWithThisProfessor(Account professor, Subject subject) throws Exception{
		AttachedSubject attachedSubject = new AttachedSubject();
		attachedSubject.setProfessor(professor);
		attachedSubject.setSubject(subject);
		em.persist(attachedSubject);
	}

	public void saveAttachedSubjectToStudent(AttachedSubject attachedSubject,
			Account student) {
		StudentSubject studentSubject = new StudentSubject();
		studentSubject.setAttachedSubject(attachedSubject);
		studentSubject.setStudent(student);
		em.persist(studentSubject);
	}

	public List<StudentSubject> getStudentSubjectsByStudentId(Long studentId) {
		return em.createQuery("from StudentSubject where studentId=:studentId", StudentSubject.class).
				setParameter("studentId", studentId).getResultList();
	}

	public void saveStudentSubject(StudentSubject studentSubject) {
		em.merge(studentSubject);
	}
	

}
