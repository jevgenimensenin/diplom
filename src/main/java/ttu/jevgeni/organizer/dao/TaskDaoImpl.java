package ttu.jevgeni.organizer.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.Task;
import ttu.jevgeni.organizer.model.TaskType;

@Repository
@Transactional
public class TaskDaoImpl implements TaskDao{
	
	@PersistenceContext
	private EntityManager em;
	
	public TaskType getTaskTypeById(Long taskTypeId) {
		return em.createQuery("from TaskType where taskTypeId = :taskTypeId", TaskType.class)
				.setParameter("taskTypeId", taskTypeId).getSingleResult();
	}

	public Task getTaskById(Long taskId) {
		return em.createQuery("from Task where taskId = :taskId", Task.class)
				.setParameter("taskId", taskId).getSingleResult();
	}

	public void delete(Long taskId) {
		Task task = getTaskById(taskId);
		em.remove(task);
	}
	
	
	
}
