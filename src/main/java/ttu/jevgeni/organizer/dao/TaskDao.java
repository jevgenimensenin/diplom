package ttu.jevgeni.organizer.dao;

import ttu.jevgeni.organizer.model.Task;
import ttu.jevgeni.organizer.model.TaskType;

public interface TaskDao {
	
	public TaskType getTaskTypeById(Long taskTypeId);
	
	public Task getTaskById(Long taskId);
	
	public void delete(Long taskId);
	
}
