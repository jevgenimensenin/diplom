package ttu.jevgeni.organizer.dao;

import ttu.jevgeni.organizer.model.Account;

public interface AccountDao {
	
	public Account getAccountById(long accountId);
	
	public Account getAccountByEmail(String email);
	
}
