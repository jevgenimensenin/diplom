package ttu.jevgeni.organizer.dao;

import ttu.jevgeni.organizer.model.StudentSubject;

public interface StudentSubjectDao {
	
	public StudentSubject getStudentSubjectById(Long studentSubjectId);
	
}
