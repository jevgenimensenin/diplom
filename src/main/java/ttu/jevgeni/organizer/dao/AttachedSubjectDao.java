package ttu.jevgeni.organizer.dao;

import java.util.List;

import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Task;

public interface AttachedSubjectDao {
	
	public void saveTasksToSubject(Task task);
	
	public AttachedSubject getAttachedSubjectById(Long attachedSubjectId);
	
	public AttachedSubject setCommentToSubject(AttachedSubject attachedSubject);
	
	public List<AttachedSubject> getAllAttachedSubjects();
	
}
