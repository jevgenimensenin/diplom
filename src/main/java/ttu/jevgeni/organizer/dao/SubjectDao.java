package ttu.jevgeni.organizer.dao;

import java.util.List;

import ttu.jevgeni.organizer.model.Subject;

public interface SubjectDao {
	
	public List<Subject> getAllSubjects();
	
	public Subject getSubjectById(long subjectId);
	
}
