package ttu.jevgeni.organizer.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ttu.jevgeni.organizer.model.Subject;

@Repository
@Transactional
public class SubjectDaoImpl implements SubjectDao{
	
	@PersistenceContext
	private EntityManager em;
	
	public List<Subject> getAllSubjects() {
		return em.createQuery("from Subject", Subject.class).getResultList();
	}

	public Subject getSubjectById(long subjectId) {
		return em.createQuery("from Subject where subjectId = :subjectId", Subject.class)
				.setParameter("subjectId", subjectId).getSingleResult();
	}
	
}
