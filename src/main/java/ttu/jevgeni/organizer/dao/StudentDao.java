package ttu.jevgeni.organizer.dao;


import java.util.List;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.StudentSubject;

public interface StudentDao {
	
	public void saveAttachedSubjectToStudent(AttachedSubject attachedSubject,Account student) throws Exception;
	
	public List<StudentSubject> getStudentSubjectsByStudentId(Long studentId);
	
	public void saveStudentSubject(StudentSubject studentSubject);
	
}
