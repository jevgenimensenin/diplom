package ttu.jevgeni.organizer.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="task")
public class Task {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskId;
	
	private String name;
	
	private Integer week;
	
	private Integer weight;
	
	@ManyToOne
	@JoinColumn(name = "tasktypeid")
	private TaskType taskType;
	
	private Long attachedSubjectId;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public TaskType getTaskType() {
		return taskType;
	}

	public void setTaskType(TaskType taskType) {
		this.taskType = taskType;
	}

	public Long getAttachedSubjectId() {
		return attachedSubjectId;
	}

	public void setAttachedSubjectId(Long attachedSubjectId) {
		this.attachedSubjectId = attachedSubjectId;
	}

	public String toString(){
		return (taskId+" "+name+" "+week+" "+weight+" "+attachedSubjectId+" "+taskType.getName());
	}

}
