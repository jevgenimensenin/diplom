package ttu.jevgeni.organizer.model;

public enum ListRole {
	PROFESSOR, STUDENT;
}
