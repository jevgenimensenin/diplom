package ttu.jevgeni.organizer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="studenttaskdetails")
public class StudentTaskDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long studentTaskGradeId;
	
	@Column(insertable = false, updatable = false)
	private Long studentSubjectId;
	
	@ManyToOne
	@JoinColumn(name = "taskId")
	private Task task;
	
	private Integer grade;
	
	private String date;
	
	private String time;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}	

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getStudentTaskGradeId() {
		return studentTaskGradeId;
	}

	public void setStudentTaskGradeId(Long studentTaskGradeId) {
		this.studentTaskGradeId = studentTaskGradeId;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Long getStudentSubjectId() {
		return studentSubjectId;
	}

	public void setStudentSubjectId(Long studentSubjectId) {
		this.studentSubjectId = studentSubjectId;
	}

	
}
