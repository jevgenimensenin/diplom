package ttu.jevgeni.organizer.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name="attachedsubject")
public class AttachedSubject {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long attachedSubjectId;
	
	@ManyToOne
	@JoinColumn(name = "professorid")
	@JsonIgnore
	private Account professor;
	
	@ManyToOne
	@JoinColumn(name = "subjectid")
	private Subject subject;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "attachedSubjectId")
	private List<Task> tasks;
	
	private String comment;
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Long getAttachedSubjectId() {
		return attachedSubjectId;
	}

	public void setAttachedSubjectId(Long attachedSubjectId) {
		this.attachedSubjectId = attachedSubjectId;
	}

	public Account getProfessor() {
		return professor;
	}

	public void setProfessor(Account professor) {
		this.professor = professor;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
}
