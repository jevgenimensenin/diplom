package ttu.jevgeni.organizer.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name="studentsubject")
public class StudentSubject {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long studentSubjectId;
	
	@ManyToOne
	@JoinColumn(name = "studentId")
	@JsonIgnore
	private Account student;
	
	@ManyToOne
	@JoinColumn(name = "attachedSubjectId")
	private AttachedSubject attachedSubject;
	
	@OneToMany(fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	@JoinColumn(name="studentSubjectId", nullable=false)
	private List<StudentTaskDetails> subjectTaskDetails;
	
	private Integer possibleGrade;
	
	public Long getStudentSubjectId() {
		return studentSubjectId;
	}

	public void setStudentSubjectId(Long studentSubjectId) {
		this.studentSubjectId = studentSubjectId;
	}

	public Account getStudent() {
		return student;
	}

	public void setStudent(Account student) {
		this.student = student;
	}

	public AttachedSubject getAttachedSubject() {
		return attachedSubject;
	}

	public void setAttachedSubject(AttachedSubject attachedSubject) {
		this.attachedSubject = attachedSubject;
	}
	
	public Integer getPossibleGrade() {
		return possibleGrade;
	}

	public void setPossibleGrade(Integer possibleGrade) {
		this.possibleGrade = possibleGrade;
	}

	public List<StudentTaskDetails> getSubjectTaskDetails() {
		return subjectTaskDetails;
	}

	public void setSubjectTaskDetails(List<StudentTaskDetails> subjectTaskDetails) {
		this.subjectTaskDetails = subjectTaskDetails;
	}

	@Override
	public String toString() {
		return "StudentSubject [studentSubjectId=" + studentSubjectId
				+ ", student=" + student + ", attachedSubject="
				+ attachedSubject + ", subjectTaskGrades=" + subjectTaskDetails
				+ ", possibleGrade=" + possibleGrade + "]";
	}
	
	
	
}
