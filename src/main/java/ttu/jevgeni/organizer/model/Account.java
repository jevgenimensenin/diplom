package ttu.jevgeni.organizer.model;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="account")
public class Account{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long accountId;
	
	private String email;
	
	private String password;
	
	private String name;
	
	private Integer code;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(name="userrole", joinColumns=@JoinColumn(name="accountid"),
	inverseJoinColumns=@JoinColumn(name="roleid"))
	private Set<UserRole> userRoles = new HashSet<UserRole>();
	//TO-DO DELETE THIS METHOD AND REFACTOR:
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "studentsubject", 
	joinColumns = { @JoinColumn(name = "studentid") }, 
	inverseJoinColumns = { @JoinColumn(name = "attachedsubjectid") })
	private List<AttachedSubject> studentSubjects;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "student")
	private List<StudentSubject> studentSubjectsWithTaskGrades;

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public List<AttachedSubject> getStudentSubjects() {
		return studentSubjects;
	}

	public void setStudentSubjects(List<AttachedSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getUsername() {
		return null;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public List<StudentSubject> getStudentSubjectsWithTaskGrades() {
		return studentSubjectsWithTaskGrades;
	}

	public void setStudentSubjectsWithTaskGrades(
			List<StudentSubject> studentSubjectsWithTaskGrades) {
		this.studentSubjectsWithTaskGrades = studentSubjectsWithTaskGrades;
	}

}
