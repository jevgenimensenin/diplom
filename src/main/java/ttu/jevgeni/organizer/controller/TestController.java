package ttu.jevgeni.organizer.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ttu.jevgeni.organizer.model.StudentSubject;

@Controller
@RequestMapping("/test")
@Transactional
public class TestController {

	@PersistenceContext
	private EntityManager em;
	
	@RequestMapping(value="/controller")
	public String showGroup() {
		em.createQuery("from TaskType").getResultList();
		em.createQuery("from Task").getResultList();
		em.createQuery("from Subject").getResultList();
		em.createQuery("from Account").getResultList();
		em.createQuery("from Account where accountId = 1").getSingleResult();
		List<StudentSubject> ss = em.createQuery("from StudentSubject where studentId = 2").getResultList();
		return "test";
	}
}
