package ttu.jevgeni.organizer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Subject;
import ttu.jevgeni.organizer.model.Task;
import ttu.jevgeni.organizer.service.AccountService;
import ttu.jevgeni.organizer.service.AttachedSubjectService;
import ttu.jevgeni.organizer.service.ProfessorService;
import ttu.jevgeni.organizer.service.SubjectService;
import ttu.jevgeni.organizer.service.TaskService;

@Controller
@RequestMapping(value="/professor")
public class ProfessorController {
	
	@Autowired
	SubjectService subjectService;
	
	@Autowired
	ProfessorService professorService;

	@Autowired
	AccountService accountService;
	
	@Autowired
	TaskService taskService;
	
	@Autowired
	AttachedSubjectService attachedSubjectService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String showProfessorPage(Model model){
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Account account = accountService.getAccountByEmail(user.getUsername());
		model.addAttribute("allSubjects", subjectService.getAllSubjects());
		model.addAttribute("professorSubjects", professorService.getAllProfessorSubjects(account.getAccountId()));
		model.addAttribute("professor", account);
		return "professor";
	}
	
	@RequestMapping(value="/edit/{attachedSubjectId}", method = RequestMethod.GET)
	public String showEditTicketPage(Model model, @PathVariable Long attachedSubjectId){
		model.addAttribute("attachedSubject", professorService.getAttachedSubjectById(attachedSubjectId));
		return "ticket";
	}

	@RequestMapping(value = "/addSubjectsToProfessor", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String addSubjectsToProfessor(@RequestParam("professorId") Long professorId,
			@RequestParam("subjectId") Long subjectId) throws Exception{
		Subject subject = subjectService.getSubjectById(subjectId);
		Account professor = accountService.getAccountById(professorId);
		try{
			professorService.saveSubjectWithThisProfessor(professor, subject);
		}catch(Exception e){
			System.out.println("this already exist");
		}
		return null;
	}
	
	@RequestMapping(value = "/saveTasksToSubject", method = RequestMethod.POST)
	public @ResponseBody String saveTasksToSubject(@RequestParam("tasks") String tasks, 
			@RequestParam("attachedSubjectId") Long attachedSubjectId, 
			@RequestParam("comment") String comment) throws Exception{
		Gson gson = new Gson();
		List<Task> tasksToDB = gson.fromJson(tasks, new TypeToken<List<Task>>(){}.getType());
		for(Task task: tasksToDB){
			task.setAttachedSubjectId(attachedSubjectId);
			attachedSubjectService.saveTasksToSubject(task);
		}
		if(!comment.equals("")){
			AttachedSubject attachedSubject = attachedSubjectService.getAttachedSubjectById(attachedSubjectId);
			attachedSubject.setComment(comment);
			attachedSubjectService.setCommentToSubject(attachedSubject);
		}
		return null;
	}
	
	@RequestMapping(value = "/getSubjectTasks", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody List<Task> getSubjectTasks(@RequestParam("attachedSubjectId") Long attachedSubjectId){
		List<Task> tasks = attachedSubjectService.getAttachedSubjectById(attachedSubjectId).getTasks();
		return tasks;
	}
	
	@RequestMapping(value = "/deleteTask", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteTask(@RequestParam("taskId") Long taskId) throws Exception{
		taskService.delete(taskId);
		return null;
	}
	
	
}
