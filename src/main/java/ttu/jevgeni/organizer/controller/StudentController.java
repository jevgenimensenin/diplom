package ttu.jevgeni.organizer.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.StudentSubject;
import ttu.jevgeni.organizer.model.StudentTaskDetails;
import ttu.jevgeni.organizer.model.Task;
import ttu.jevgeni.organizer.service.AccountService;
import ttu.jevgeni.organizer.service.AttachedSubjectService;
import ttu.jevgeni.organizer.service.StudentService;
import ttu.jevgeni.organizer.service.StudentSubjectService;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	AttachedSubjectService attachedSubjectService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	StudentService studentService;
 
	@Autowired
	StudentSubjectService studentSubjectService;
	
    @RequestMapping(method = RequestMethod.GET)
    public String studentProfile(Model model){
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Account account = accountService.getAccountByEmail(user.getUsername());
    	HashMap<String, List<Task>> thisWeekTasks = studentService.getThisWeekTasks(account.getAccountId(), 5);
    	studentService.calculatePossibleGrade(account);
    	model.addAttribute("thisWeekTasks", thisWeekTasks);
    	model.addAttribute("student", account);
        return "studentProfile";
    }
    
    @RequestMapping(value = "/addSubjectToStudent", method = RequestMethod.POST)
	public @ResponseBody String addSubjectsToProfessor(@RequestParam("attachedSubjectId") Long attachedSubjectId,
			@RequestParam("studentId") Long studentId) throws SQLException{
		AttachedSubject attachedSubject = attachedSubjectService.getAttachedSubjectById(attachedSubjectId);
		Account student = accountService.getAccountById(studentId);
		studentService.saveAttachedSubjectToStudent(attachedSubject, student);
		return null;
	}
    
    @RequestMapping(value = "/getTasksByWeek", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String getTasksByWeek(@RequestParam("week") Integer week, @RequestParam("studentId") Long studentId){
		HashMap<String, List<Task>> thisWeekTasks = studentService.getThisWeekTasks(studentId, week);
		Gson gson = new Gson();
		String thisWeekTasksJson = gson.toJson(thisWeekTasks);
		return thisWeekTasksJson;
	}
    
    @RequestMapping(value="/{studentId}/subjects", method = RequestMethod.GET)
	public String showEditTicketPage(Model model, @PathVariable("studentId") Long studentId){
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Account account = accountService.getAccountByEmail(user.getUsername());
    	model.addAttribute("studentId", account.getAccountId());
    	model.addAttribute("attachedSubjects", attachedSubjectService.getAllAttachedSubjects());
		return "studentAddSubject";
	}
    
    @RequestMapping(value = "/getStudentSubject", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody StudentSubject getSubjectTasks(@RequestParam("studentSubjectId") Long studentSubjectId){
		StudentSubject studentSubject = studentSubjectService.getStudentSubjectById(studentSubjectId);
		return studentSubject;
	}
    
    @RequestMapping(value = "/getStudentSubjects", method = RequestMethod.POST, produces = "application/json")
   	public @ResponseBody List<StudentSubject> getStudent(@RequestParam("studentId") Long studentId){
    	List<StudentSubject> studentSubjects = accountService.getAccountById(studentId).getStudentSubjectsWithTaskGrades();
   		return studentSubjects;
   	}
    
    @RequestMapping(value = "/makeCalendarExportFile", method = RequestMethod.POST)
	public @ResponseBody String makeCalendarExportFile(@RequestParam("studentId") Long studentId) throws SQLException{
		Account student = accountService.getAccountById(studentId);
		studentService.makeCalendarExportFile(student);
		return null;
	}
}
