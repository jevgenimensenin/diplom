package ttu.jevgeni.organizer.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.AccountDao;
import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.UserRole;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	AccountDao accountDao;
	
	@SuppressWarnings("deprecation")
	@Transactional
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException {
		Account account = accountDao.getAccountByEmail(email);
		if(account!=null){
			String password = account.getPassword();
			
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			for(UserRole userRole: account.getUserRoles()){
				authorities.add(new GrantedAuthorityImpl(userRole.getName().name()));
			}
			org.springframework.security.core.userdetails.User securityUser = new
					org.springframework.security.core.userdetails.User(email, password, authorities);
			return securityUser;
		}else{
			throw new UsernameNotFoundException("No such username");
		}
	}

}
