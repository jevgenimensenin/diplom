package ttu.jevgeni.organizer.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.StudentSubjectDao;
import ttu.jevgeni.organizer.model.StudentSubject;

@Service
public class StudentSubjectServiceImpl implements StudentSubjectService {
	
	@Autowired
	StudentSubjectDao studentSubjectDao;

	
	
	public StudentSubject getStudentSubjectById(long studentSubjectId) {
		return studentSubjectDao.getStudentSubjectById(studentSubjectId);
	}

	
}

