package ttu.jevgeni.organizer.service;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.StudentDao;
import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.StudentSubject;
import ttu.jevgeni.organizer.model.StudentTaskDetails;
import ttu.jevgeni.organizer.model.Task;

@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	StudentDao studentDao;

	public void saveAttachedSubjectToStudent(AttachedSubject attachedSubject,
			Account student) {
		try {
			studentDao.saveAttachedSubjectToStudent(attachedSubject, student);
		} catch (Exception e) {
			System.out.println("Stud-Subj unique!");
		}
	}

	public HashMap<String, List<Task>> getThisWeekTasks(Long studentId, Integer week) {
		List<StudentSubject> studentSubjects = studentDao.getStudentSubjectsByStudentId(studentId);
		HashMap<String, List<Task>> thisWeekTasksAndSubjects = new HashMap<String, List<Task>>();
		for(StudentSubject studentSubject : studentSubjects){
			List<Task> thisWeekTasks = new ArrayList<Task>();
			for(Task task:studentSubject.getAttachedSubject().getTasks()){
				if(task.getWeek()==week){
					thisWeekTasks.add(task);
				}
			}
			if(thisWeekTasks.size()!=0){
				thisWeekTasksAndSubjects.put(studentSubject.getAttachedSubject().getSubject().getName(),
						thisWeekTasks);
			}
		}
		return thisWeekTasksAndSubjects;
	}

	public void calculatePossibleGrade(Account account) {
		for(StudentSubject studentSubject: account.getStudentSubjectsWithTaskGrades()){
			int currentGrades = 0;
			int remainigWeight = 0;
			for(Task task : studentSubject.getAttachedSubject().getTasks()){
				remainigWeight += task.getWeight();
			}
			for(StudentTaskDetails subjectGrade : studentSubject.getSubjectTaskDetails()){
				if(subjectGrade.getGrade()!=null){
					currentGrades += (subjectGrade.getTask().getWeight() * 0.01 * subjectGrade.getGrade());
					remainigWeight -= subjectGrade.getTask().getWeight();
				}
			}
			int possibleGrade = remainigWeight + currentGrades;
			for(int i=51; i<111; i+=10){
				if(possibleGrade<i){
					studentSubject.setPossibleGrade((i/10) - 5);
					studentDao.saveStudentSubject(studentSubject);
					break;
				}
			}
		}
	}

	public void makeCalendarExportFile(Account student) {
		try
		{	
		    FileWriter writer = new FileWriter("C:/Users/Jevgeni/Desktop/test.csv");
		    
		    writer.append("Subject");writer.append(',');
		    writer.append("Start Date");writer.append(',');
		    writer.append("Start Time");writer.append(',');
		    writer.append("End Date");writer.append(',');
		    writer.append("End Time");writer.append(',');
		    writer.append("All day event");writer.append(',');
		    writer.append("Description");writer.append(',');
		    writer.append("Location");writer.append(',');
		    writer.append('\n');
		    
		    for(StudentSubject studentSubject: student.getStudentSubjectsWithTaskGrades()){
		    	for(StudentTaskDetails task: studentSubject.getSubjectTaskDetails()){
		    		writer.append(task.getTask().getName()); writer.append(',');
		    		writer.append(task.getDate());writer.append(',');
		    		writer.append(task.getTime());writer.append(',');
		    		writer.append(task.getDate());writer.append(',');
		    		writer.append(task.getTime());writer.append(',');
		    		writer.append("FALSE");writer.append(',');
		    		writer.append("Description");writer.append(',');
		    		writer.append("TTU");writer.append(',');
		    		writer.append('\n');
		    	}
		    }	 
		    writer.flush();
		    writer.close();
		}
		catch(IOException e)
		{
		     e.printStackTrace();
		} 
	    }
	}


