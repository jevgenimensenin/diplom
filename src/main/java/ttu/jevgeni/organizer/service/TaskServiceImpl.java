package ttu.jevgeni.organizer.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.TaskDao;
import ttu.jevgeni.organizer.model.Task;

@Service
public class TaskServiceImpl implements TaskService {
	
	@Autowired
	TaskDao taskDao;

	public Task getTaskById(Long taskId) {
		return taskDao.getTaskById(taskId);
	}

	public void delete(Long taskId) {
		taskDao.delete(taskId);
	}

	
}
