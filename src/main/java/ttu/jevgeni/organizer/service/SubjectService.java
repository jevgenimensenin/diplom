package ttu.jevgeni.organizer.service;

import java.util.List;

import ttu.jevgeni.organizer.model.Subject;

public interface SubjectService {
	
	public List<Subject> getAllSubjects();
	
	public Subject getSubjectById(long subjectId);
	
}
