package ttu.jevgeni.organizer.service;

import java.util.List;

import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Task;

public interface AttachedSubjectService {
	public void saveTasksToSubject(Task task);
	
	public AttachedSubject getAttachedSubjectById(long attachedSubjectId);
	
	public AttachedSubject setCommentToSubject(AttachedSubject attachedSubject);
	
	public List<AttachedSubject> getAllAttachedSubjects();
}
