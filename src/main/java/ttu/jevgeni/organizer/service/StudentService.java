package ttu.jevgeni.organizer.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Task;

public interface StudentService {
	
	public void saveAttachedSubjectToStudent(AttachedSubject attachedSubject, Account student) throws SQLException;
	
	public HashMap<String, List<Task>> getThisWeekTasks(Long stidentId, Integer week);
	
	public void calculatePossibleGrade(Account account);
	
	public void makeCalendarExportFile(Account student);
	
}
