package ttu.jevgeni.organizer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.SubjectDao;
import ttu.jevgeni.organizer.model.Subject;

@Service
public class SubjectServiceImpl implements SubjectService {
	
	@Autowired
	SubjectDao subjectDao;

	public List<Subject> getAllSubjects() {
		return subjectDao.getAllSubjects();
	}

	public Subject getSubjectById(long subjectId) {
		return subjectDao.getSubjectById(subjectId);
	}
	
}
