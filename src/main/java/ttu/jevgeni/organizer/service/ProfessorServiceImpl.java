package ttu.jevgeni.organizer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.ProfessorDao;
import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Subject;

@Service
public class ProfessorServiceImpl implements ProfessorService {
	
	@Autowired
	ProfessorDao professorDao;

	public List<AttachedSubject> getAllProfessorSubjects(long professorId) {
		return professorDao.getAllProfessorSubjects(professorId);
	}

	public void saveSubjectWithThisProfessor(Account professor, Subject subject) throws Exception {
		professorDao.saveSubjectWithThisProfessor(professor, subject);
	}

	public AttachedSubject getAttachedSubjectById(Long attachedSubjectId) {
		return professorDao.getAttachedSubjectById(attachedSubjectId);
	}
}

