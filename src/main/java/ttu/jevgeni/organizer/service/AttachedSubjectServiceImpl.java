package ttu.jevgeni.organizer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.AttachedSubjectDao;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Task;

@Service
public class AttachedSubjectServiceImpl implements AttachedSubjectService {
	
	@Autowired
	AttachedSubjectDao attachedSubjectDao;

	public void saveTasksToSubject(Task task) {
		attachedSubjectDao.saveTasksToSubject(task);
	}

	public AttachedSubject getAttachedSubjectById(long attachedSubjectId) {
		return attachedSubjectDao.getAttachedSubjectById(attachedSubjectId);
	}

	public AttachedSubject setCommentToSubject(AttachedSubject attachedSubject) {
		return attachedSubjectDao.setCommentToSubject(attachedSubject);
	}

	public List<AttachedSubject> getAllAttachedSubjects() {
		return attachedSubjectDao.getAllAttachedSubjects();
	}
	
	
}

