package ttu.jevgeni.organizer.service;


import ttu.jevgeni.organizer.model.Account;

public interface AccountService {
	
	public Account getAccountById(long accountId);
	
	public Account getAccountByEmail(String email);
	
}
