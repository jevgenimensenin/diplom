package ttu.jevgeni.organizer.service;

import ttu.jevgeni.organizer.model.Task;

public interface TaskService {
	
	public Task getTaskById(Long taskId);
	
	public void delete(Long taskId);
	
}
