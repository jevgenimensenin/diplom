package ttu.jevgeni.organizer.service;

import ttu.jevgeni.organizer.model.StudentSubject;

public interface StudentSubjectService {
	
	public StudentSubject getStudentSubjectById(long studentSubjectId);
}
