package ttu.jevgeni.organizer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttu.jevgeni.organizer.dao.AccountDao;
import ttu.jevgeni.organizer.model.Account;

@Service
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	AccountDao accountDao;

	public Account getAccountById(long accountId) {
		return accountDao.getAccountById(accountId);
	}

	public Account getAccountByEmail(String email) {
		return accountDao.getAccountByEmail(email);
	}
	
}
