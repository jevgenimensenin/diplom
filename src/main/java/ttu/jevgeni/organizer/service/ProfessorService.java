package ttu.jevgeni.organizer.service;

import java.util.List;

import ttu.jevgeni.organizer.model.Account;
import ttu.jevgeni.organizer.model.AttachedSubject;
import ttu.jevgeni.organizer.model.Subject;

public interface ProfessorService {
	
	public List<AttachedSubject> getAllProfessorSubjects(long professorId);
	
	public void saveSubjectWithThisProfessor(Account professor, Subject subject) throws Exception;
	
	public AttachedSubject getAttachedSubjectById(Long attachedSubjectId);
}
