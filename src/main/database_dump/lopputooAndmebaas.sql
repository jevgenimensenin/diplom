--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2015-01-15 20:34:42

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 188 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2048 (class 0 OID 0)
-- Dependencies: 188
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 176 (class 1259 OID 41156)
-- Name: account; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE account (
    accountid bigint NOT NULL,
    email character varying(100),
    password character varying(100),
    name character varying,
    code integer
);


ALTER TABLE public.account OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 41170)
-- Name: attachedsubject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE attachedsubject (
    attachedsubjectid bigint NOT NULL,
    professorid bigint,
    subjectid bigint,
    comment character varying
);


ALTER TABLE public.attachedsubject OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 41173)
-- Name: attachedsubjects_attachedsubjectid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE attachedsubjects_attachedsubjectid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attachedsubjects_attachedsubjectid_seq OWNER TO postgres;

--
-- TOC entry 2049 (class 0 OID 0)
-- Dependencies: 179
-- Name: attachedsubjects_attachedsubjectid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE attachedsubjects_attachedsubjectid_seq OWNED BY attachedsubject.attachedsubjectid;


--
-- TOC entry 183 (class 1259 OID 57459)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE role (
    roleid bigint NOT NULL,
    name character varying(20)
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 57457)
-- Name: role_userroleid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE role_userroleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_userroleid_seq OWNER TO postgres;

--
-- TOC entry 2050 (class 0 OID 0)
-- Dependencies: 182
-- Name: role_userroleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE role_userroleid_seq OWNED BY role.roleid;


--
-- TOC entry 180 (class 1259 OID 41181)
-- Name: studentsubject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE studentsubject (
    studentsubjectid bigint NOT NULL,
    studentid bigint,
    attachedsubjectid bigint,
    possiblegrade integer
);


ALTER TABLE public.studentsubject OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 41184)
-- Name: studentsubject_studentsubjectid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE studentsubject_studentsubjectid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studentsubject_studentsubjectid_seq OWNER TO postgres;

--
-- TOC entry 2051 (class 0 OID 0)
-- Dependencies: 181
-- Name: studentsubject_studentsubjectid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE studentsubject_studentsubjectid_seq OWNED BY studentsubject.studentsubjectid;


--
-- TOC entry 187 (class 1259 OID 73862)
-- Name: studenttaskdetails; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE studenttaskdetails (
    studenttaskgradeid bigint NOT NULL,
    studentsubjectid bigint,
    taskid bigint,
    grade integer,
    date character varying,
    "time" character varying
);


ALTER TABLE public.studenttaskdetails OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 73860)
-- Name: studenttaskgrade_studenttaskgradeid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE studenttaskgrade_studenttaskgradeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studenttaskgrade_studenttaskgradeid_seq OWNER TO postgres;

--
-- TOC entry 2052 (class 0 OID 0)
-- Dependencies: 186
-- Name: studenttaskgrade_studenttaskgradeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE studenttaskgrade_studenttaskgradeid_seq OWNED BY studenttaskdetails.studenttaskgradeid;


--
-- TOC entry 175 (class 1259 OID 41111)
-- Name: subject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subject (
    subjectid bigint NOT NULL,
    name character varying(100),
    speciality character varying(9)
);


ALTER TABLE public.subject OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 41109)
-- Name: subject_subjectid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE subject_subjectid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subject_subjectid_seq OWNER TO postgres;

--
-- TOC entry 2053 (class 0 OID 0)
-- Dependencies: 174
-- Name: subject_subjectid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE subject_subjectid_seq OWNED BY subject.subjectid;


--
-- TOC entry 173 (class 1259 OID 41082)
-- Name: task; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE task (
    taskid bigint NOT NULL,
    name character varying(255),
    week integer,
    weight integer,
    tasktypeid bigint,
    attachedsubjectid bigint
);


ALTER TABLE public.task OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 41080)
-- Name: task_task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE task_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_task_id_seq OWNER TO postgres;

--
-- TOC entry 2054 (class 0 OID 0)
-- Dependencies: 172
-- Name: task_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE task_task_id_seq OWNED BY task.taskid;


--
-- TOC entry 170 (class 1259 OID 41066)
-- Name: tasktype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tasktype (
    tasktypeid bigint NOT NULL,
    name character varying
);


ALTER TABLE public.tasktype OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 41069)
-- Name: tasktype_tasktype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tasktype_tasktype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasktype_tasktype_id_seq OWNER TO postgres;

--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 171
-- Name: tasktype_tasktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tasktype_tasktype_id_seq OWNED BY tasktype.tasktypeid;


--
-- TOC entry 177 (class 1259 OID 41159)
-- Name: user_userid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_userid_seq OWNER TO postgres;

--
-- TOC entry 2056 (class 0 OID 0)
-- Dependencies: 177
-- Name: user_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_userid_seq OWNED BY account.accountid;


--
-- TOC entry 184 (class 1259 OID 57465)
-- Name: userrole; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE userrole (
    userroleid bigint NOT NULL,
    accountid bigint,
    roleid bigint
);


ALTER TABLE public.userrole OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 57468)
-- Name: userrole_userroleid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE userrole_userroleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.userrole_userroleid_seq OWNER TO postgres;

--
-- TOC entry 2057 (class 0 OID 0)
-- Dependencies: 185
-- Name: userrole_userroleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE userrole_userroleid_seq OWNED BY userrole.userroleid;


--
-- TOC entry 1878 (class 2604 OID 41161)
-- Name: accountid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY account ALTER COLUMN accountid SET DEFAULT nextval('user_userid_seq'::regclass);


--
-- TOC entry 1879 (class 2604 OID 41175)
-- Name: attachedsubjectid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attachedsubject ALTER COLUMN attachedsubjectid SET DEFAULT nextval('attachedsubjects_attachedsubjectid_seq'::regclass);


--
-- TOC entry 1881 (class 2604 OID 57462)
-- Name: roleid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role ALTER COLUMN roleid SET DEFAULT nextval('role_userroleid_seq'::regclass);


--
-- TOC entry 1880 (class 2604 OID 41186)
-- Name: studentsubjectid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY studentsubject ALTER COLUMN studentsubjectid SET DEFAULT nextval('studentsubject_studentsubjectid_seq'::regclass);


--
-- TOC entry 1883 (class 2604 OID 73865)
-- Name: studenttaskgradeid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY studenttaskdetails ALTER COLUMN studenttaskgradeid SET DEFAULT nextval('studenttaskgrade_studenttaskgradeid_seq'::regclass);


--
-- TOC entry 1877 (class 2604 OID 41114)
-- Name: subjectid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subject ALTER COLUMN subjectid SET DEFAULT nextval('subject_subjectid_seq'::regclass);


--
-- TOC entry 1876 (class 2604 OID 41085)
-- Name: taskid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY task ALTER COLUMN taskid SET DEFAULT nextval('task_task_id_seq'::regclass);


--
-- TOC entry 1875 (class 2604 OID 41071)
-- Name: tasktypeid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasktype ALTER COLUMN tasktypeid SET DEFAULT nextval('tasktype_tasktype_id_seq'::regclass);


--
-- TOC entry 1882 (class 2604 OID 57470)
-- Name: userroleid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY userrole ALTER COLUMN userroleid SET DEFAULT nextval('userrole_userroleid_seq'::regclass);


--
-- TOC entry 2029 (class 0 OID 41156)
-- Dependencies: 176
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY account (accountid, email, password, name, code) FROM stdin;
50	f	\N	\N	\N
1	admin	8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918	Professor One	111222
4	admin2	8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918	Viktor Zdravstvui	124547
2	student	264c8c381bf16c982a4e59b0dd4c6f7808c51a05f64c35db42cc78a2a72875bb	Jevgeni Mensenin	112656
3	student2	264c8c381bf16c982a4e59b0dd4c6f7808c51a05f64c35db42cc78a2a72875bb	Andrus Tandrus	133258
\.


--
-- TOC entry 2031 (class 0 OID 41170)
-- Dependencies: 178
-- Data for Name: attachedsubject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY attachedsubject (attachedsubjectid, professorid, subjectid, comment) FROM stdin;
117	1	1	All homeworks, tests and exam should be done to pass this subject
125	1	6	sddsv fd dsfvdff ve dfv fds c 
124	1	5	I write important information about this subject. Do not forget, do not be afraid
127	1	8	All homeworks, test 1, lab 1, lab 2 should be done to pass the
128	1	7	\N
131	1	9	\N
134	4	3	\N
135	4	5	\N
136	4	7	\N
138	1	2	Test Comment
\.


--
-- TOC entry 2058 (class 0 OID 0)
-- Dependencies: 179
-- Name: attachedsubjects_attachedsubjectid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('attachedsubjects_attachedsubjectid_seq', 139, true);


--
-- TOC entry 2036 (class 0 OID 57459)
-- Dependencies: 183
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role (roleid, name) FROM stdin;
1	PROFESSOR
2	STUDENT
\.


--
-- TOC entry 2059 (class 0 OID 0)
-- Dependencies: 182
-- Name: role_userroleid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('role_userroleid_seq', 2, true);


--
-- TOC entry 2033 (class 0 OID 41181)
-- Dependencies: 180
-- Data for Name: studentsubject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY studentsubject (studentsubjectid, studentid, attachedsubjectid, possiblegrade) FROM stdin;
50	2	117	5
112	2	124	3
111	2	125	0
119	2	128	0
113	2	127	5
\.


--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 181
-- Name: studentsubject_studentsubjectid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('studentsubject_studentsubjectid_seq', 124, true);


--
-- TOC entry 2040 (class 0 OID 73862)
-- Dependencies: 187
-- Data for Name: studenttaskdetails; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY studenttaskdetails (studenttaskgradeid, studentsubjectid, taskid, grade, date, "time") FROM stdin;
4	50	115	90	31.10.2014	10:00 AM
5	50	116	80	1.11.2014	11:00 AM
10	50	113	\N	2.11.2014	3:00 PM
11	50	114	\N	3.11.2014	4:00 PM
\.


--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 186
-- Name: studenttaskgrade_studenttaskgradeid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('studenttaskgrade_studenttaskgradeid_seq', 14, true);


--
-- TOC entry 2028 (class 0 OID 41111)
-- Dependencies: 175
-- Data for Name: subject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY subject (subjectid, name, speciality) FROM stdin;
3	Databases II	IDU0230
1	Object-oriented Programming in the Java Language	IDK0051
4	Automated Testing	IAY0361
5	Logic Programming	ITI0021
6	Software Engineering	IDK0071
7	User Interfaces	ITV0130
8	Advanced Programming	ITV0101
9	Web Services	IDU0075
2	Databases I	IDU0220
\.


--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 174
-- Name: subject_subjectid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('subject_subjectid_seq', 9, true);


--
-- TOC entry 2026 (class 0 OID 41082)
-- Dependencies: 173
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY task (taskid, name, week, weight, tasktypeid, attachedsubjectid) FROM stdin;
113	Homework 1 - Presentation 	3	15	1	117
114	Homework 2 - Birds And Animals	6	15	1	117
115	Homework 3 - Spoon	9	15	1	117
116	Test 1 - Basic or not	10	20	2	117
117	Lab 1  - Hard one	14	10	3	117
119	Homework 1 - Logic 	5	20	1	124
120	Homework 2 - Logic 2	10	20	1	124
153	Homework 1 - test	3	20	1	138
154	Homework 2 - test	3	20	1	138
157	Exam - All topics	\N	40	4	138
155	Test 4 - test	6	20	2	138
136	Lab 1 - test	5	10	3	125
135	Test 2 - test	5	12	2	125
130	Exam - test	\N	29	4	127
128	Lab 2 - test	11	1	3	127
127	Lab 1 - test	11	20	3	127
126	Test 1 - test	10	20	2	127
123	Exam - Only last 10 lectures	\N	40	4	124
118	Exam about everything	\N	25	4	117
158	Homework 1 - Thread factory	6	15	1	127
159	Test 2 - week 4, week 5 lectures 	6	10	2	127
\.


--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 172
-- Name: task_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('task_task_id_seq', 159, true);


--
-- TOC entry 2023 (class 0 OID 41066)
-- Dependencies: 170
-- Data for Name: tasktype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tasktype (tasktypeid, name) FROM stdin;
1	Homework
2	Test
3	Lab
4	Exam
\.


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 171
-- Name: tasktype_tasktype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tasktype_tasktype_id_seq', 4, true);


--
-- TOC entry 2065 (class 0 OID 0)
-- Dependencies: 177
-- Name: user_userid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_userid_seq', 4, true);


--
-- TOC entry 2037 (class 0 OID 57465)
-- Dependencies: 184
-- Data for Name: userrole; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY userrole (userroleid, accountid, roleid) FROM stdin;
1	1	1
2	2	2
4	3	2
5	4	1
\.


--
-- TOC entry 2066 (class 0 OID 0)
-- Dependencies: 185
-- Name: userrole_userroleid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('userrole_userroleid_seq', 5, true);


--
-- TOC entry 1891 (class 2606 OID 57487)
-- Name: account_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY account
    ADD CONSTRAINT account_email_key UNIQUE (email);


--
-- TOC entry 1895 (class 2606 OID 41180)
-- Name: attachedsubject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY attachedsubject
    ADD CONSTRAINT attachedsubject_pkey PRIMARY KEY (attachedsubjectid);


--
-- TOC entry 1897 (class 2606 OID 49259)
-- Name: attachedsubject_professorid_subjectid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY attachedsubject
    ADD CONSTRAINT attachedsubject_professorid_subjectid_key UNIQUE (professorid, subjectid);


--
-- TOC entry 1903 (class 2606 OID 57464)
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (roleid);


--
-- TOC entry 1899 (class 2606 OID 41197)
-- Name: studentsubject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY studentsubject
    ADD CONSTRAINT studentsubject_pkey PRIMARY KEY (studentsubjectid);


--
-- TOC entry 1901 (class 2606 OID 65643)
-- Name: studentsubject_studentid_attachedsubjectid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY studentsubject
    ADD CONSTRAINT studentsubject_studentid_attachedsubjectid_key UNIQUE (studentid, attachedsubjectid);


--
-- TOC entry 1907 (class 2606 OID 73867)
-- Name: studenttaskgrade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY studenttaskdetails
    ADD CONSTRAINT studenttaskgrade_pkey PRIMARY KEY (studenttaskgradeid);


--
-- TOC entry 1889 (class 2606 OID 41116)
-- Name: subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (subjectid);


--
-- TOC entry 1887 (class 2606 OID 41087)
-- Name: task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY task
    ADD CONSTRAINT task_pkey PRIMARY KEY (taskid);


--
-- TOC entry 1885 (class 2606 OID 41079)
-- Name: tasktype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tasktype
    ADD CONSTRAINT tasktype_pkey PRIMARY KEY (tasktypeid);


--
-- TOC entry 1893 (class 2606 OID 41166)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY account
    ADD CONSTRAINT user_pkey PRIMARY KEY (accountid);


--
-- TOC entry 1905 (class 2606 OID 57475)
-- Name: userrole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY userrole
    ADD CONSTRAINT userrole_pkey PRIMARY KEY (userroleid);


--
-- TOC entry 1910 (class 2606 OID 41260)
-- Name: studentsubject_attachedsubjectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY studentsubject
    ADD CONSTRAINT studentsubject_attachedsubjectid_fkey FOREIGN KEY (attachedsubjectid) REFERENCES attachedsubject(attachedsubjectid);


--
-- TOC entry 1911 (class 2606 OID 41275)
-- Name: studentsubject_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY studentsubject
    ADD CONSTRAINT studentsubject_studentid_fkey FOREIGN KEY (studentid) REFERENCES account(accountid);


--
-- TOC entry 1915 (class 2606 OID 73878)
-- Name: studenttaskgrade_studentsubjectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY studenttaskdetails
    ADD CONSTRAINT studenttaskgrade_studentsubjectid_fkey FOREIGN KEY (studentsubjectid) REFERENCES studentsubject(studentsubjectid);


--
-- TOC entry 1914 (class 2606 OID 73873)
-- Name: studenttaskgrade_taskid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY studenttaskdetails
    ADD CONSTRAINT studenttaskgrade_taskid_fkey FOREIGN KEY (taskid) REFERENCES task(taskid);


--
-- TOC entry 1909 (class 2606 OID 49260)
-- Name: task_attachedsubjectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY task
    ADD CONSTRAINT task_attachedsubjectid_fkey FOREIGN KEY (attachedsubjectid) REFERENCES attachedsubject(attachedsubjectid);


--
-- TOC entry 1908 (class 2606 OID 41088)
-- Name: task_tasktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY task
    ADD CONSTRAINT task_tasktype_id_fkey FOREIGN KEY (tasktypeid) REFERENCES tasktype(tasktypeid);


--
-- TOC entry 1912 (class 2606 OID 57476)
-- Name: userrole_accountid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY userrole
    ADD CONSTRAINT userrole_accountid_fkey FOREIGN KEY (accountid) REFERENCES account(accountid);


--
-- TOC entry 1913 (class 2606 OID 57481)
-- Name: userrole_roleid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY userrole
    ADD CONSTRAINT userrole_roleid_fkey FOREIGN KEY (roleid) REFERENCES role(roleid);


--
-- TOC entry 2047 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-01-15 20:34:42

--
-- PostgreSQL database dump complete
--

