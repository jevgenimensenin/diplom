var tasks = [];
var weightOfAlreadyCreatedTasks = 0;
function saveHomework(){
	var taskType={taskTypeId:1, name:"Homework"};
	var homework = {name: $("#homeworkDesc").val(), 
			week: $("#homeworkWeek").val(), weight: $("#homeworkWeight").val(), taskType:taskType};
	tasks.push(homework);
	appendHomework(homework.name, homework.week, homework.weight);
}

function saveTest(){
	var taskType={taskTypeId:2};
	var test = {name: $("#testDesc").val(), week: $("#testWeek").val(), weight: $("#testWeight").val(), taskType:taskType};
	tasks.push(test);
	appendTest(test.name, test.week, test.weight);
}

function saveLab(){
	var taskType={taskTypeId:3};
	var lab = {name: $("#labDesc").val(), week: $("#labWeek").val(), weight: $("#labWeight").val(), taskType:taskType};
	tasks.push(lab);
	appendLab(lab.name, lab.week, lab.weight);
}

function saveExam(){
	var taskType={taskTypeId:4};
	var exam = {name: $("#examDesc").val(), weight: $("#examWeight").val(), taskType:taskType};
	tasks.push(exam);
	appendExam(exam.name, exam.weight)
}

function saveTicket(attachedSubjectId){
	$.post("/organizer/professor/saveTasksToSubject", 
			{ tasks:JSON.stringify(tasks), attachedSubjectId: attachedSubjectId, comment:$('#ticketComment').val()}, function(callback){
		location.reload();
	});
}

function getSubjectTasks(attachedSubjectId){
	$.ajax({
		url: "/organizer/professor/getSubjectTasks",
		type: 'POST',
		async : false,
		data : {attachedSubjectId : attachedSubjectId},
		success: function( callback ) {
			console.log(callback);
			for(var i=0; i<callback.length; i++){
				if(callback[i].taskType.taskTypeId==1){
					appendHomework(callback[i].name, callback[i].week, callback[i].weight, callback[i].taskId)
				}else if(callback[i].taskType.taskTypeId==2){
					appendTest(callback[i].name, callback[i].week, callback[i].weight, callback[i].taskId)
				}else if(callback[i].taskType.taskTypeId==3){
					appendLab(callback[i].name, callback[i].week, callback[i].weight, callback[i].taskId)
				}else{
					appendExam(callback[i].name, callback[i].weight, callback[i].taskId)
				}
			}	
		}
		});
}

function deleteTask(taskId, weight){
	$.post("/organizer/professor/deleteTask", {taskId:taskId});
	weightOfAlreadyCreatedTasks = weightOfAlreadyCreatedTasks-parseInt(weight);
	$('.progress-bar').css("width", (weightOfAlreadyCreatedTasks * 0.01 * $('.progress').width()));
}

function appendHomework(name, week, weight, taskId){
	weightOfAlreadyCreatedTasks = weightOfAlreadyCreatedTasks+parseInt(weight);
	$('.progress-bar').css("width", (weightOfAlreadyCreatedTasks * 0.01 * $('.progress').width()));
	$('#newTicket').append('<div class="alert alert-dismissable alert-info">'
			  +'<button type="button" onclick="deleteTask('+taskId+','+weight+')" class="close" data-dismiss="alert">×</button>'
			  +'<strong>'+name+'&nbsp;</strong>'+week+' week&nbsp;<strong>'+weight+' %</strong>'
			  +'</div>');
}

function appendTest(name, week, weight, taskId){
	weightOfAlreadyCreatedTasks = weightOfAlreadyCreatedTasks+parseInt(weight);
	$('.progress-bar').css("width", (weightOfAlreadyCreatedTasks * 0.01 * $('.progress').width()));
	$('#newTicket').append('<div class="alert alert-dismissable alert-info" id="ticketTest">'
			  +'<button type="button" onclick="deleteTask('+taskId+','+weight+')" class="close" data-dismiss="alert">×</button>'
			  +'<strong>'+name+'&nbsp;</strong>'+week+' week&nbsp;<strong>'+weight+' %</strong>'
			  +'</div>');
}

function appendLab(name, week, weight, taskId){
	weightOfAlreadyCreatedTasks = weightOfAlreadyCreatedTasks+parseInt(weight);
	$('.progress-bar').css("width", (weightOfAlreadyCreatedTasks * 0.01 * $('.progress').width()));
	$('#newTicket').append('<div class="alert alert-dismissable alert-info" id="ticketLab">'
			  +'<button type="button" onclick="deleteTask('+taskId+','+weight+')" class="close" data-dismiss="alert">×</button>'
			  +'<strong>'+name+'&nbsp;</strong>'+week+' week&nbsp;<strong>'+weight+' %</strong>'
			  +'</div>');
}

function appendExam(name, weight, taskId){
	weightOfAlreadyCreatedTasks = weightOfAlreadyCreatedTasks+parseInt(weight);
	$('.progress-bar').css("width", (weightOfAlreadyCreatedTasks * 0.01 * $('.progress').width()));
	$('#newTicket').append('<div class="alert alert-dismissable alert-warning">'
			  +'<button type="button" onclick="deleteTask('+taskId+','+weight+')" class="close" data-dismiss="alert">×</button>'
			  +'<strong>'+name+'&nbsp;'+weight+'%</strong>'
			  +'</div>');
}