function showAttachedSubjectTicket(subjectId, comment, subjectName){
	$('#clickedStudentSubject').empty();
	$('#clickedStudentSubject').append('<h4 id="studentSubjectTicketName">'+subjectName+'</h4>');
	$('#clickedStudentSubject').append('<div class="alert alert-dismissable alert-info" id="studentTicketComment">'
			  +comment
			  +'</div>');
	getSubjectTasks(subjectId);
}

function getStudentSubject(studentSubjectId, comment, subjectName){
	$.ajax({
		url: "/organizer/student/getStudentSubject",
		type: 'POST',
		async : false,
		data : {studentSubjectId : studentSubjectId},
		success: function(callback) {
		}
		});
}

function makeExportFile(studentId, student){
	$('#clickedStudentSubject').empty();
	$('#clickedStudentSubject').append('<h4 id="taskToExportLabel">Fill Task dates to make export:</h4>');
	$('#clickedStudentSubject').append('<div id="subjectsToFillDate"></div>');
	$.ajax({
		url: "/organizer/student/getStudentSubjects",
		type: 'POST',
		async : false,
		data : {studentId : studentId},
		success: function( callback ) {
			$('#subjectsToFillDate').append('<p class="text-danger">'
					+'! Only task with filled dates will be exported.</p>');
			for(var i=0; i<callback.length; i++){
				$('#subjectsToFillDate').append('<a class="clickableButton" onclick="showSubjectForExportTasks('+callback[i].studentSubjectId+')"><b>'+callback[i].attachedSubject.subject.name+'</b></a>'
						+' ('+callback[i].subjectTaskDetails.length+'/'+callback[i].attachedSubject.tasks.length+' filled)</br>');
				
			}
			$('#subjectsToFillDate').append('<div id="containerToFillDates"></div>');
			showSubjectForExportTasks(callback[0].studentSubjectId)
		}
		});
	generateCalendarCSV(studentId);
	$('#subjectsToFillDate').append('<a href="#" class="btn btn-primary">Submit</a>');
}

function showSubjectForExportTasks(studentSubjectId){
	$('#containerToFillDates').empty();
	$.ajax({
		url: "/organizer/student/getStudentSubject",
		type: 'POST',
		async : false,
		data : {studentSubjectId : studentSubjectId},
		success: function( callback ) {
			console.log(callback);
			for(var i=0; i<callback.attachedSubject.tasks.length; i++){
				var task = callback.attachedSubject.tasks[i];
				var taskDate = null;
				if(callback.subjectTaskDetails[i]!=null){
					taskDate = callback.subjectTaskDetails[i].date;
				}
				if(task.taskType.taskTypeId==1){
					appendHomeworkForExport(task.name, task.week, task.weight, task.taskId, taskDate)
				}else if(task.taskType.taskTypeId==2){
					appendTestForExport(task.name, task.week, task.weight, task.taskId, taskDate)
				}else if(task.taskType.taskTypeId==3){
					appendLabForExport(task.name, task.week, task.weight, task.taskId, taskDate)
				}else{
					appendExamForExport(task.name, task.weight, task.taskId, taskDate)
				}
			}	
		}
		});
}

function generateCalendarCSV(id){
	$.post("/organizer/student/makeCalendarExportFile", 
			{studentId:studentId});
}

function getSubjectPossibleGrade(subjectTaskGrades){
}

function addSubjectToStudent(attachedSubjectId, studentId){
	$.post("/organizer/student/addSubjectToStudent", 
			{attachedSubjectId:attachedSubjectId, studentId:studentId}, function(callback){
				
			});
	$('.progress-bar').css("width", (weightOfAlreadyCreatedTasks * 0.01 * $('.progress').width()));
}

function getNextWeekTasks(studentId){
	week = parseInt($('#weekNumber').text())+1;
	if(week<=16){
		$('#weekNumber').empty();
		$('#weekNumber').append(week);
		getTasksByWeek(studentId, week)
	}
}

function getPreviousWeekTasks(studentId){
	week = parseInt($('#weekNumber').text())-1;
	if(week>=0){
		$('#weekNumber').empty();
		$('#weekNumber').append(week);
		getTasksByWeek(studentId, week)
	}
}

function getTasksByWeek(studentId, week){
	$.ajax({
		url: "/organizer/student/getTasksByWeek",
		type: 'POST',
		async : false,
		data : {studentId:studentId, week : week},
		success: function( callback ){
			$('#choseWeekTasks').empty();
			for (var key in callback) {
				$('#choseWeekTasks').append('<h6>'+key+'</h6>');
				for(var i=0; i<callback[key].length; i++){
					$('#choseWeekTasks').append('<div id="thisWeekTaskNames">'
							+callback[key][i].name+'</div>');
				}
			}
		}
	});
}


