jQuery(document).ready(function($) {
	$(".clickableRow.href").click(function() {
        window.document.location = $(this).attr("href");
	});
});

function addSubjectToProfessor(professorId, subjectId){
	 $.get( "/organizer/professor/addSubjectsToProfessor", {professorId: professorId, subjectId: subjectId},
		function(callback) {
		 	location.reload();
		});
};