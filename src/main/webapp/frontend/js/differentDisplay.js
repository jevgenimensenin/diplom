//Student Tasks:
function appendHomework(name, week, weight, taskId){
	$('#clickedStudentSubject').append('<div id="studentTaskContainer"><div id="studentSubjectTask">'
			  +'<strong>'+name+'&nbsp;</strong>'+week+' week&nbsp;<strong>'+weight+' %</strong>'
			  +'</div><div id="taskDoneButton"><i class="fa fa-check"></i></div>'+
			  '</div>');
}

function appendTest(name, week, weight, taskId){
	$('#clickedStudentSubject').append('<div id="studentTaskContainer"><div id="studentSubjectTask">'
			  +'<strong>'+name+'&nbsp;</strong>'+week+' week&nbsp;<strong>'+weight+' %</strong>'
			  +'</div><div id="taskDoneButton"><i class="fa fa-check"></i></div>'+
	  			'</div>');
}

function appendLab(name, week, weight, taskId){
	$('#clickedStudentSubject').append('<div id="studentTaskContainer"><div id="studentSubjectTask">'
			  +'<strong>'+name+'&nbsp;</strong>'+week+' week&nbsp;<strong>'+weight+' %</strong>'
			  +'</div><div id="taskDoneButton"><i class="fa fa-check"></i></div>'+
				'</div>');
}

function appendExam(name, weight, taskId){
	$('#clickedStudentSubject').append('<div id="studentTaskContainer"><div id="studentSubjectTask">'
			  +'<strong>'+name+'&nbsp;'+weight+' %</strong>'
			  +'</div><div id="taskDoneButton"><i class="fa fa-check"></i></div>'+
				'</div>');
}

//Tasks for Export:
function appendDateIconOrDateValue(name, week, taskId, date){
	if(date!=null){
		$('#containerToFillDates').append('<div id="studentTaskContainer"><div id="studentTasksWithFilledDate" >'
				 +name+': '+week+' week'
				  +'</div><div id="taskCalendarFilledButtonInput" style="font-size:12px">'+date.substring(0, date.length - 5)+'</div></div>');
		}else{
		$('#containerToFillDates').append('<div id="studentTaskContainer"><div id="studentTasksWithNotFilledDate">'
				 +name+': '+week+' week'
				  +'</div><div id="taskCalendarNotFilledButtonInput" onclick="showDatePicker()" style="font-size:20px"><i class="fa fa-calendar"></i></div></div>');
	}
}
function appendHomeworkForExport(name, week, weight, taskId,date){
	appendDateIconOrDateValue(name, week, taskId, date);
}

function appendTestForExport(name, week, weight, taskId, date){
	appendDateIconOrDateValue(name, week, taskId, date);
}

function appendLabForExport(name, week, weight, taskId, date){
	appendDateIconOrDateValue(name, week, taskId, date);
}

function appendExamForExport(name, week, taskId, date){
	appendDateIconOrDateValue(name, week, taskId, date);
}
function showDatePicker(){
	console.log("hey");
	$('#datePickerModal').modal();
	$('#datePickerInput').datepicker();
}
