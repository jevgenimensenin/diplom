<html>
<head>
<%@include file="libs.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/frontend/js/professor.js" />"></script>
<script src="<c:url value="/frontend/js/student.js" />"></script>
<title>Student</title>
</head>
<body>
<div id="studentAttachedSubjects">
	<table class="table table-striped table-hover ">
	  <legend>All Subjects</legend>
	  <thead>
	    <tr>
	      <th>Subject</th>
	      <th>Course code</th>
	      <th>Professor Name</th>	      
	    </tr>
	  </thead>
	  <tbody>
	    <c:forEach items="${attachedSubjects}" var="attachedSubject">
			<tr  >
				<td><a onclick="showAttachedSubjectTicket(${attachedSubject.attachedSubjectId},'${attachedSubject.comment}')">
				${attachedSubject.subject.name}</a></td>
				<td>${attachedSubject.subject.speciality}</td>
				<td>${attachedSubject.professor.name}</td>
				<td><a onclick="addSubjectToStudent(${attachedSubject.attachedSubjectId}, ${studentId})">add to my subject</a></td>
			</tr>
		</c:forEach>
	  </tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal" id="addSubjectModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body" id="studentTicket">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>