<html>
<head>
<%@include file="libs.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/frontend/js/professor.js" />"></script>
<script src="<c:url value="/frontend/js/student.js" />"></script>
<script src="<c:url value="/frontend/js/differentDisplay.js" />"></script>
<script src="<c:url value="/frontend/js/jquery.timepicker.js" />"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<title>Student</title>
</head>
<body>
	<div id="studentLeftBar" class="col-md-4">
		<br>
		${student.name} ${student.code}
		<br><br>
		<b id="thisWeekTaskLabel">This Week Tasks</b>
		<br>
		<c:forEach items="${thisWeekTasks}" var="thisWeekTasks">
		   <h6>${thisWeekTasks.key}</h6>
		   <div id="thisWeekTaskNames">
		   <c:forEach items="${thisWeekTasks.value}" var="thisWeekTask">
		   		${thisWeekTask.name} <br>
			</c:forEach>
			</div>
		</c:forEach>
		<ul class="pager">
		  <li><a class="clickableButton" onclick="getPreviousWeekTasks(${student.accountId})">Previous</a></li>
		  <li><b id="weekNumberTasks">Week <a id="weekNumber">5</a> tasks</b></li>
		  <li><a class="clickableButton" onclick="getNextWeekTasks(${student.accountId})">Next</a></li>
		</ul>
		<div id=choseWeekTasks></div>
	</div>
	<script type="text/javascript">
		var studentId = ${student.accountId};
		getNextWeekTasks(studentId);
	</script>
<table id="studentSubjects" class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>Subject</th>
      <th>Professor</th>
      <th>Possible grade</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${student.studentSubjectsWithTaskGrades}" var="studentSubject">
		<tr >
			<td id="subjectNameInStudentTable"><a onclick="showAttachedSubjectTicket(${studentSubject.attachedSubject.attachedSubjectId},
			'${studentSubject.attachedSubject.comment}', '${studentSubject.attachedSubject.subject.name}')">
			${studentSubject.attachedSubject.subject.speciality} - ${studentSubject.attachedSubject.subject.name}</a></td> 
			<!-- <td id="subjectNameInStudentTable"><a onclick="getStudentSubject(${studentSubject.studentSubjectId},
			'${studentSubject.attachedSubject.comment}', '${studentSubject.attachedSubject.subject.name}')">
			${studentSubject.attachedSubject.subject.speciality} - ${studentSubject.attachedSubject.subject.name}</a></td>
			-->
			<td>${studentSubject.attachedSubject.professor.name}</td>
			<td>${studentSubject.possibleGrade}</td>
		</tr>
	</c:forEach>
  </tbody>
</table>
<a id="exportToCalendarButton" onclick="makeExportFile(${student.accountId})" class="btn btn-default">Export to Calendar</a>
<div id="clickedStudentSubject"></div>
<div class="modal" id="datePickerModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h4 class="modal-title">Select Date and time</h4>
      </div>
      <div class="modal-body">
         <label class="control-label" for="focusedInput">Date:</label>
         <input class="form-control" id="datePickerInput" type="text" value="This is focused...">
         </br>
          <label class="control-label" for="focusedInput">Time: (optional)</label>
         <input class="form-control" id="timePickerInput" type="text" value="This is focused...">         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>