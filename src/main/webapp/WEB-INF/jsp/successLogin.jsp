<html>
<head>
<%@include file="libs.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
<sec:authentication property="name"/> <sec:authentication property="authorities"/> 
<br />
<sec:authorize access="hasRole('PROFESSOR')">
	<c:redirect url="/professor"/>
</sec:authorize>
<sec:authorize access="hasRole('STUDENT')">
	<c:redirect url="/student"/>
</sec:authorize>
</body>
</html>