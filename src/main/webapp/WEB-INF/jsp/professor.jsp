<html>
<head>
<%@include file="libs.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/frontend/js/professor.js" />"></script>
<title>Professor</title>
</head>
<body>
<!--<sec:authentication property="name"/><sec:authentication property="authorities"/> -->
<sec:authorize access="hasRole('PROFESSOR')">
<br />

<div id="professorTables">
<div id="allSubjects">
	<table class="table table-striped table-hover ">
	  <legend>All Subjects</legend>
	  <thead>
	    <tr>
	      <th>Subject</th>
	      <th>Course code</th>
	    </tr>
	  </thead>
	  <tbody>
	    <c:forEach items="${allSubjects}" var="subject">
				<tr class="clickableRow" onclick="addSubjectToProfessor(${professor.accountId}, ${subject.subjectId})">
				<td>${subject.name}</td>
				<td>${subject.speciality}</td>
			</tr>
		</c:forEach>
	  </tbody>
	</table>
</div>
	<div id="professorSubjects">
	<table class="table table-striped table-hover ">
	<legend>My Subjects</legend>
	  <thead>
	    <tr>
	      <th>Subject</th>
	      <th>Course code</th>
	    </tr>
	  </thead>
	  <tbody>
	    <c:forEach items="${professorSubjects}" var="professorSubject">
			<tr class="clickableRow href" href="/organizer/professor/edit/${professorSubject.attachedSubjectId}">
				<td>${professorSubject.subject.name}</td>
				<td>${professorSubject.subject.speciality}</td>
			</tr>
		</c:forEach>
	  </tbody>
	</table>
</div>
</div>
</sec:authorize>
</body>
</html>