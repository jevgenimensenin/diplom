<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/frontend/style/login.css" />"
		rel="stylesheet">
<title>Login</title>
</head>
<body>
<div class="login">
	<div class="login-screen">
		<div class="app-title">
				<h1>Login</h1>
		</div>
			<div class="login-form">
			<form action="j_spring_security_check" method="POST">
				<div class="control-group">
				<input type="text" name="j_username" class="login-field" value="" placeholder="username" id="login-name">
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>

				<div class="control-group">
				<input type="password" name="j_password" class="login-field" value="" placeholder="password" id="login-pass">
				<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<input class="btn btn-primary btn-large btn-block" type="submit" value="Login"/>
				<a class="login-link" href="#">Register</a>
			</form>
			</div>
		</div>
	</div>
</body>
</html>