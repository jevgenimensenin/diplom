<html>
<head>
<%@include file="libs.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/frontend/js/professor.js" />"></script>
<title>Professor</title>
</head>
<body>
<div class="well bs-component" id="newForm">
	<form class="form-horizontal">
	  <fieldset>
	    <legend>${attachedSubject.subject.name}</legend>
	    <div class="progress progress-striped active">
		  <div class="progress-bar"></div>
		</div>
	    <div class="form-group">
	      <label for="inputEmail" class="col-lg-2 control-label">Homework</label>
	      <div class="col-lg-10">
			  <div class="input-group" id="addLab">
			    <input type="text" class="form-control" id="homeworkDesc" placeholder="Description">
			     <span class="input-group-btn">
			    	<select class="btn formDropdown" id="homeworkWeek">
			          <option disabled selected>Week</option>
			          <c:forEach begin="1" end="16" var="val">
						    <option>${val}</option>
					  </c:forEach>
				        </select>
				        <select class="btn formDropdown" id="homeworkWeight">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" onclick="saveHomework()" type="button" >+</button>
			    </span>
			  </div>
		   </div>
		</div>
		<div class="form-group">
	      <label for="inputEmail" class="col-lg-2 control-label">Test</label>
	      <div class="col-lg-10">
			  <div class="input-group" id="addLab">
			    <input type="text" class="form-control" id="testDesc" placeholder="Description">
			    <span class="input-group-btn">
			    	<select class="btn formDropdown" id="testWeek">
			          <option disabled selected>Week</option>
			          <c:forEach begin="1" end="16" var="val">
						    <option>${val}</option>
					  </c:forEach>
				        </select>
				        <select class="btn formDropdown" id="testWeight">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" type="button" onclick="saveTest()">+</button>
			    </span>
			  </div>
		   </div>
		</div>
	    <div class="form-group">
	      <label for="inputEmail" class="col-lg-2 control-label">Lab</label>
	      <div class="col-lg-10">
			  <div class="input-group" id="addLab">
			    <input type="text" class="form-control" id="labDesc" placeholder="Description">
			    <span class="input-group-btn">
			    	<select class="btn formDropdown" id="labWeek">
			          <option disabled selected>Week</option>
			          <c:forEach begin="1" end="16" var="val">
						    <option>${val}</option>
					  </c:forEach>
				        </select>
				        <select class="btn formDropdown" id="labWeight">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" type="button" onclick="saveLab()">+</button>
			    </span>
			  </div>
		   </div>
		</div>
	    <div class="form-group">
	      <label for="inputPassword" class="col-lg-2 control-label">Examination session</label>
	      <div class="col-lg-10">
	      	<div class="checkbox">
	          <label>
	            <input type="checkbox"> Exam
	          </label>
	        </div>
	        <div class="input-group" id="addLab">
			    <input type="text" class="form-control" id="examDesc" placeholder="Description">
			    <span class="input-group-btn">
				        <select class="btn formDropdown" id="examWeight">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" type="button" onclick="saveExam()">+</button>
			    </span>
			  </div>
	      </div>
	    </div>
	      <div class="form-group">
	      <label for="textArea" class="col-lg-2 control-label">Comment</label>
	      <div class="col-lg-10">
	        <textarea class="form-control" rows="3" id="ticketComment"></textarea>
	        <span class="help-block">Subject comment</span>
	      </div>
	    </div>
	    <div class="form-group">
	      <div class="col-lg-10 col-lg-offset-2">
	        <button class="btn btn-default">Cancel</button>
	        <button type="button" class="btn btn-primary" onclick="saveTicket(${attachedSubject.attachedSubjectId})">Submit Ticket</button>
	      </div>
	    </div>
	  </fieldset>
	</form>
</div>
<div class="well bs-component" id="newTicket">
	<div class="panel panel-primary">
		  <div class="panel-body">
		    ${attachedSubject.comment}
		  </div>
	</div>
	<script type="text/javascript">
		var attachedSubjectId = ${attachedSubject.attachedSubjectId};
		getSubjectTasks(attachedSubjectId);
	</script>
</div>
</body>
</html>