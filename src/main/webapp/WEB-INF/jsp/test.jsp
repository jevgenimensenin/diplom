<html>
<head>
	<%@include file="libs.jsp"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<div class="well bs-component" id="newForm">
	<form class="form-horizontal">
	  <fieldset>
	    <legend>Object Oriented Java</legend>
	    <div class="progress progress-striped active">
		  <div class="progress-bar" style="width: 65%"></div>
		</div>
	    <div class="form-group">
	      <label for="inputEmail" class="col-lg-2 control-label">Homework</label>
	      <div class="col-lg-10">
			  <div class="input-group" id="addLab">
			    <input type="text" class="form-control" id="homeworkDesc" placeholder="Description">
			     <span class="input-group-btn">
			    	<select class="btn formDropdown" id="homeworkWeek">
			          <option disabled selected>Week</option>
			          <c:forEach begin="1" end="16" var="val">
						    <option>${val}</option>
					  </c:forEach>
				        </select>
				        <select class="btn formDropdown" id="homeworkWeight">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" onclick="saveHomework()" type="button" >+</button>
			    </span>
			  </div>
		   </div>
		</div>
		<div class="form-group">
	      <label for="inputEmail" class="col-lg-2 control-label">Test</label>
	      <div class="col-lg-10">
			  <div class="input-group" id="addLab">
			    <input type="text" class="form-control" placeholder="Description">
			    <span class="input-group-btn">
			    	<select class="btn formDropdown">
			          <option disabled selected>Week</option>
			          <c:forEach begin="1" end="16" var="val">
						    <option>${val}</option>
					  </c:forEach>
				        </select>
				        <select class="btn formDropdown">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" type="button" >+</button>
			    </span>
			  </div>
		   </div>
		</div>
	    <div class="form-group">
	      <label for="inputEmail" class="col-lg-2 control-label">Lab</label>
	      <div class="col-lg-10">
			  <div class="input-group" id="addLab">
			    <input type="text" class="form-control" placeholder="Description">
			    <span class="input-group-btn">
			    	<select class="btn formDropdown">
			          <option disabled selected>Week</option>
			          <c:forEach begin="1" end="16" var="val">
						    <option>${val}</option>
					  </c:forEach>
				        </select>
				        <select class="btn formDropdown">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" type="button">+</button>
			    </span>
			  </div>
		   </div>
		</div>
	    <div class="form-group">
	      <label for="inputPassword" class="col-lg-2 control-label">Examination session</label>
	      <div class="col-lg-10">
	      	<div class="checkbox">
	          <label>
	            <input type="checkbox"> Exam
	          </label>
	        </div>
	        <div class="input-group" id="addLab">
			    <input type="text" class="form-control" placeholder="Description">
			    <span class="input-group-btn">
				        <select class="btn formDropdown">
				          <option disabled selected>%</option>
				          <c:forEach begin="1" end="100" var="val">
							  <option>${val}</option>
						  </c:forEach>
				        </select>
			      <button class="btn btn-primary" type="button" value="12">+</button>
			    </span>
			  </div>
	      </div>
	    </div>
	      <div class="form-group">
	      <label for="textArea" class="col-lg-2 control-label">Comment</label>
	      <div class="col-lg-10">
	        <textarea class="form-control" rows="3" id="textArea"></textarea>
	        <span class="help-block">Subject comment</span>
	      </div>
	    </div>
	    <div class="form-group">
	      <div class="col-lg-10 col-lg-offset-2">
	        <button class="btn btn-default">Cancel</button>
	        <button type="submit" class="btn btn-primary">Submit Ticket</button>
	      </div>
	    </div>
	  </fieldset>
	</form>
</div>
<div class="well bs-component" id="newTicket">
	<h2>Ticket</h2>
	<div class="alert alert-dismissable alert-info">
	  <button type="button" class="close" data-dismiss="alert">�</button>
	  <strong>Homework 1 - Presentation</strong> 3 week <strong>10%</strong>
	</div>
	<div class="alert alert-dismissable alert-info">
	  <button type="button" class="close" data-dismiss="alert">�</button>
	  <strong>Homework 2 - Bird Factory</strong> 6 week <strong>10%</strong>
	</div>
	<div class="alert alert-dismissable alert-info">
	  <button type="button" class="close" data-dismiss="alert">�</button>
	  <strong>Homework 3 - Exceptions</strong> 9 week <strong>10%</strong>
	</div>
	<div class="alert alert-dismissable alert-info" id="ticketTest">
	  <button type="button" class="close" data-dismiss="alert">�</button>
	  <strong>Test 1 - Incapsulation</strong> 9 week <strong>30%</strong>
	</div>
	<div class="alert alert-dismissable alert-info" id="ticketlab">
	  <button type="button" class="close" data-dismiss="alert">�</button>
	  <strong>Lab 1 - Working with Java</strong> 14 week <strong>5%</strong>
	</div>
	<div class="alert alert-dismissable alert-warning">
	  <button type="button" class="close" data-dismiss="alert">�</button>
	  <strong>Exam - Encapsulation, Polymorphism, Abstraction 35%</strong>
	</div>
</div>
</body>
</html>
