<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- CSS -->
	<link href="<c:url value="/frontend/style/font-awesome.css" />"
		rel="stylesheet/less">
	<link href="<c:url value="/frontend/style/main.css" />"
		rel="stylesheet">
	<link href="<c:url value="/frontend/style/bootstrap.css" />"
		rel="stylesheet">
	<link href="<c:url value="/frontend/style/bootswatch.less" />"
		rel="stylesheet/less">
	<link href="<c:url value="/frontend/style/variables.less" />"
		rel="stylesheet/less">
		
	<!-- JS -->
	<script src="<c:url value="/frontend/js/jquery-2.1.1.js" />"></script>
	<script src="<c:url value="/frontend/js/bootstrap.js" />"></script>
	<script src="<c:url value="/frontend/js/ticket.js" />"></script>
</head>
<body>
	<div class="navbar navbar-inverse">
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <sec:authorize access="hasRole('PROFESSOR')">
	    	<a class="navbar-brand" href="/organizer/professor">Professor</a>
	    </sec:authorize>
	    <sec:authorize access="hasRole('STUDENT')">
	    	<a class="navbar-brand" href="/organizer/professor">Student</a>
	    </sec:authorize>
	  </div>
	  <div class="navbar-collapse collapse navbar-inverse-collapse">
	    <ul class="nav navbar-nav">
	    <sec:authorize access="hasRole('STUDENT')">
	      <li class="active"><a href="/organizer/student">Profile</a></li>
	      <li><a href="/organizer/student/${student.accountId}/subjects">Add Subject</a></li>
	      </sec:authorize>
	      <li class="dropdown">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<b class="caret"></b></a>
	        <ul class="dropdown-menu">
	          <li><a href="#">Action</a></li>
	          <li><a href="#">Another action</a></li>
	          <li><a href="#">Something else here</a></li>
	          <li class="divider"></li>
	          <li class="dropdown-header">Dropdown header</li>
	          <li><a href="#">Separated link</a></li>
	          <li><a href="#">One more separated link</a></li>
	        </ul>
	      </li>
	    </ul>
	    <form class="navbar-form navbar-left">
	      <input type="text" class="form-control col-lg-8" placeholder="Search">
	    </form>
	    <ul class="nav navbar-nav navbar-right">
	      <li><a href="/organizer/logout">Logout</a></li>
	    </ul>
	  </div>
	</div>
</body>
</html>
